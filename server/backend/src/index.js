const express = require('express');
const app = express();
const port = 3000;
const facesFileSave = "../saves/faces.json";
const faces = require(facesFileSave);
const cors = require("cors")

const save = () => {
    const fs = require("fs");
    fs.writeFileSync(facesFileSave)
}

console.log("Init cors")
app.use(cors());

console.log("open GET /faces")
app.get('/faces', (req, res) => {
    res.json(faces);
})

console.log("open GET /faces/:id")
app.get('/faces/:id', (req, res) => {
    console.log("GET faces with id " + req.params.id);
    const face = faces.find((x) => x.id == req.params.id);
    if (!face) {
        res.status(404);
        res.send(`No face found with id ${req.params.id}`);
        return;
    }
    res.json(face);
})

console.log("open POST /faces/:id")
app.post('/faces/:id', (req, res) => {
    const index = faces.findIndexOf((x) => x.id == req.params.id);
    if (index == -1) {
        res.status(404);
        res.send(`No face found with id ${req.params.id}`);
        return;
    }
    faces[index] = req.body.face;
})

app.listen(port, () => {
    console.log("Server launch listen to port " + port);
})