import axios from "axios";
import config from "../../config.json";

const requestConfig = {}

export default {
    get: (path) => {
        return axios.get(config.backend + path, requestConfig);
    },
    post: (path, data) => {
        return axios.post(config.backend + path, data, requestConfig);
    },
}