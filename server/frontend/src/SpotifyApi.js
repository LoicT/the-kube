import SpotifyWebApi from 'spotify-web-api-node'

export const SpotifyApi = {
    spotifyApi : new SpotifyWebApi({
      clientId: '8934efc9a911440eb195979da8fc09b9',
      clientSecret: 'd62fae4591b843f9acf1ee1c581658fd',
      redirectUri: 'http://localhost:8080/#/spotify'
    }),
  
    play : function() {
      this.spotifyApi.play();
    },
    pause : function() {
      this.spotifyApi.pause();
    }
  }
   
  