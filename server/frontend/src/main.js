import Vue from 'vue'
import App from './App.vue'
import { MdCard, MdButton, MdToolbar, MdIcon, MdDrawer, MdList, MdContent, MdField, MdTable } from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import VueRouter from 'vue-router'
import Home from './components/Home.vue'
import Spotify from './components/Spotify.vue'
import FaceEdit from './components/FaceEdit.vue'


Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(MdCard)
Vue.use(MdButton)
Vue.use(MdToolbar)
Vue.use(MdIcon)
Vue.use(MdDrawer)
Vue.use(MdList)
Vue.use(MdContent)
Vue.use(MdField)
Vue.use(MdTable)

const routes = [
  { path: "/", component: Home },
  { path: "/spotify", component: Spotify },
  { path: "/face/:id", component: FaceEdit }
]

const router = new VueRouter({
  routes
})


new Vue({
  render: h => h(App),
  router
}).$mount('#app')
