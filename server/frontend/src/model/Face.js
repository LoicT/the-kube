import FaceController from "../controller/FaceController.js";

export class Face {
    constructor(id, name) {
        this.id = id;
        this.name = name;
        this.description = "";
        this.angle = null;
        this.behaviours = []
    }
}


export const Faces = []//require('../config/Face.json')
FaceController.get('/faces').then((res) => {
    console.log(res)
})