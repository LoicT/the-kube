import { SpotifyApi } from "../SpotifyApi";


export class Behaviour {
    constructor(id, name, description, action) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.action = action;
    }
}

export const Behaviours = [
    new Behaviour(1,"play,","lancer le lecteur",SpotifyApi.play),
    new Behaviour(2,"pause,","mettre en pause le lecteur",SpotifyApi.pause)
]
