#define RX 4
#define TX 5

#include <Wire.h>
#include <Adafruit_MMA8451.h>
#include <Adafruit_Sensor.h>
#include <SoftwareSerial.h>

SoftwareSerial bluetooth(RX,TX);
Adafruit_MMA8451 mma = Adafruit_MMA8451();

void setup(void) {
  Serial.begin(115200);
  bluetooth.begin(115200);

  if (! mma.begin()) {
    Serial.println("Couldnt start");
    while (1);
  }
  Serial.println("MMA8451 found!");
  
  mma.setRange(MMA8451_RANGE_2_G);
  
  Serial.print("Range = "); Serial.print(2 << mma.getRange());  
  Serial.println("G");
    
  // Configuration of Pmod BT2 as slave (Bluetooth module)
  bluetooth.print("$");
  bluetooth.print("$");
  bluetooth.print("$");
  delay(100);
  bluetooth.print("SM,0\r");
  bluetooth.print("SA,0\r");
  bluetooth.print("R,1\r");
  bluetooth.print("---\r");
}

uint8_t previousOrientation = 8; // Possible returned values by the accelerometer are 0 to 7
int count = 0;

void loop() {

  /* Get a new sensor event */ 
  sensors_event_t event; 
  mma.getEvent(&event);

  /* Display the results (acceleration is measured in m/s^2) and send it via Bluetooth module if accelerometer is stable */
  Serial.print("X: \t"); Serial.print(event.acceleration.x); Serial.print("\t");
  Serial.print("Y: \t"); Serial.print(event.acceleration.y); Serial.print("\t");
  Serial.print("Z: \t"); Serial.print(event.acceleration.z); Serial.print("\t");
  Serial.println("m/s^2 ");

  if (isStable()) {
    /*
     * Floats are basically 4 unsigned integers. In order to transmit them in bytes, we split them
     * into arrays of bytes and then send them via Bluetooth module.
     */
    uint8_t *x;
    int i; // index of iterator
    
    x = (uint8_t*)(&event.acceleration.x);
    for(i = 0; i < sizeof(x); i++) {
      bluetooth.write(x[i]);
    }
    uint8_t *y;
    y = (uint8_t*)(&event.acceleration.y);
    for(i = 0; i < sizeof(x); i++) {
      bluetooth.write(y[i]);
    }
    uint8_t *z;
    z = (uint8_t*)(&event.acceleration.z);
    for(i = 0; i < sizeof(x); i++) {
      bluetooth.write(z[i]);
    }
  } else {
    Serial.println("The Kube is not stable.");
  }

  delay(500);
  
}

boolean isStable() {
  uint8_t currentOrientation = mma.getOrientation();
  if (currentOrientation == previousOrientation) {
    if (count > 2) {
      count = 0;
      return true;
    }
    count++;
  } else {
    previousOrientation = currentOrientation;
    count = 0;
  }
  return false;
}
